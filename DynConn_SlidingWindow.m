%% Dynamic connectivity analysis (Sliding window approach)
clear;clc

HeadPath = strcat('/store4/bypark/26.Samsung_fMRI/ICA');

% Basic settings
TR = 3;
NumDim = 70;
num_perm = 5000;
beta = 6;               % scale-free index
wsize = 22;             % To capture the slowest frequency (0.009Hz -> 111s), but in this case, 66s (22 TRs)
sigma = 3;              % Gaussian kernel
num_repetitions = 10;   % number of repetitions of L1 regularization
grp1_name = 'grp1_pat';
grp2_name = 'grp2_nor';
grp1_case = 16;
grp2_case = 16;
total_case = grp1_case + grp2_case;

ConnPath = strcat(HeadPath,'/NumDim_',int2str(NumDim),'/Dynamic');
ConnPath_temp_result = strcat(ConnPath,'/perm_',int2str(num_perm));
mkdir(ConnPath_temp_result);

% Load ROI lists
[num,txt,raw] = xlsread(strcat(HeadPath,'/NumDim_',int2str(NumDim),'/IC_interpret/IC_hand_labeling.xlsx'),'Re-order');
ICs_signal_num = num(:,1);  % list the good nodes (counting starts at 1, not 0)
ICs_signal = [1:length(ICs_signal_num)]';
Nroi = length(ICs_signal);

% Compute connectivity matrix
flist = dir(strcat(HeadPath,'/NumDim_',int2str(NumDim),'/TimeSeries/*.txt'));

OrigMat_stack = cell(length(flist), 1);
RegMat_stack = cell(length(flist), 1);
best_lambda_stack = zeros(length(flist), 1);
netparam_stack = cell(length(flist), 4);    % hubness, BC, DC, EVC
for list = 1:length(flist)
    disp(strcat(['list = ',int2str(list),' -- ',flist(list).name]));
    
    varnorm = 1;
    
    grotALL=load(strcat(HeadPath,'/NumDim_',int2str(NumDim),'/TimeSeries/',flist(list).name));
    gn=size(grotALL,1);
    
    ts.NtimepointsPerSubject=gn;
    
    grot=grotALL(1:gn,:);
    grot=grot-repmat(mean(grot),size(grot,1),1); % demean
    if varnorm==1
        grot=grot/std(grot(:)); % normalise whole subject stddev
    elseif varnorm==2
        grot=grot ./ repmat(std(grot),size(grot,1),1); % normalise each separate timeseries from each subject
    end
    TS=grot;    
    
    ts.ts=TS;
    ts.tr=TR;
    ts.Nsubjects=1;
    ts.Nnodes=size(TS,2);
    ts.NnodesOrig=ts.Nnodes;
    ts.Ntimepoints=size(TS,1);
    ts.NtimepointsPerSubject=ts.NtimepointsPerSubject;
    ts.DD=1:ts.Nnodes;
    ts.UNK=[];
    
    %%% cleanup and remove bad nodes' timeseries (whichever is not listed in ts.DD is *BAD*).
    ts.DD = ICs_signal;
    ts = nets_tsclean(ts,1);	% regress the bad nodes out of the good, and then remove the bad nodes' timeseries (1=aggressive, 0=unaggressive (just delete bad)).
    
    Nwin = ts.NtimepointsPerSubject - wsize + 1;
    [OrigMat, RegMat, best_lambda] = MyDynConn(ts.ts, Nroi, wsize, sigma, num_repetitions);
    OrigMat_stack{list,1} = OrigMat;
    RegMat_stack{list,1} = RegMat;
    best_lambda_stack(list,1) = best_lambda;
    
    disp('              Compute network parameters: ');
    fprintf('                Nwin = %d: ', Nwin);
    Hubness = zeros(Nwin, Nroi);    % Compute dynamic connectivity matrices
    BC = zeros(Nwin, Nroi);
    DC = zeros(Nwin, Nroi);
    EVC = zeros(Nwin, Nroi);
    for slid = 1:1:Nwin
        if rem(slid,100) == 0
            fprintf('%d.. ', slid)
        end
        wrcm = RegMat(:,:,slid);
        
        [B, D, E] = NetworkCentrality(wrcm);
        % Betweenness centrality
        Hubness(slid,:) = B(:,2)';
        BC(slid,:) = B(:,1)';
        % Degree centrality
        DC(slid,:) = D';
        % Eigenvector coefficient
        EVC(slid,:) = E';
    end
    netparam_stack{list,1} = Hubness;
    netparam_stack{list,2} = BC;
    netparam_stack{list,3} = DC;
    netparam_stack{list,4} = EVC;
    fprintf('\n')
end
cd(ConnPath);
save('OrigMat_stack.mat','OrigMat_stack'); save('RegMat_stack.mat','RegMat_stack'); save('best_lambda_stack.mat','best_lambda_stack');
save('netparam_stack.mat','netparam_stack');






%%%%%%%%%%%%%%%%%%% Find the optimal number of clusters %%%%%%%%%%%%%%%%%%%
max_cluster = 10;
cluster_results = cell(length(flist),1);
for Nsub = 1:length(flist)
    disp(strcat(['Subject: ',int2str(Nsub)]));
    
    RegMat_3D = RegMat_stack{Nsub,1};
    RegMat_2D = reshape(RegMat_3D,[size(RegMat_3D,1)*size(RegMat_3D,2), size(RegMat_3D,3)])';
    
    % silhouette method
    silh = zeros(max_cluster,1);
    disp('  Calculating silhouette coefficiet');
    for num_cluster = 2:max_cluster
        idx = kmeans(RegMat_2D,num_cluster);
        silh(num_cluster,1) = mean(silhouette(RegMat_2D, idx));
    end
    [temp, k_silh] = max(silh);
    
    % elbow method
    distortion = zeros(max_cluster+1,1);
    disp('  Performing elbow method');
    for num_cluster = 1:max_cluster+1
        %     disp(strcat(['Iteration (number of clusters): ',int2str(num_cluster),' / ',int2str(max_cluster+1)]));
        
        [~,~,sumd] = kmeans(RegMat_2D,num_cluster,'emptyaction','drop');
        destortion_temp = sum(sumd);
        % try differnet tests to find minimun disortion under k_temp clusters
        for test_count = 2:max_cluster
            [~,~,sumd] = kmeans(RegMat_2D,num_cluster,'emptyaction','drop');
            destortion_temp = min(destortion_temp,sum(sumd));
        end
        distortion(num_cluster,1)=destortion_temp;
    end
    variance=distortion(1:end-1)-distortion(2:end);
    distortion_percent=cumsum(variance)/(distortion(1)-distortion(end));
    [r,~] = find(distortion_percent>0.9);
    k_elbow = r(1,1);
    
    OptK = floor(mean([k_silh, k_elbow]));
    disp(strcat(['  "K_Silhouette: ',int2str(k_silh),'", "K_Elbow: ',int2str(k_elbow),'", "Optimal K: ',int2str(OptK),'"']));
    
    
    IDX_itr = zeros(size(RegMat_3D,3), 10);
    C_itr = zeros(OptK, size(RegMat_3D,1)*size(RegMat_3D,2), 10);
    SUMD_itr = zeros(OptK,10);
    D_itr = zeros(size(RegMat_3D,3), OptK, 10);
    for itr = 1:10
        [IDX, C, SUMD, D] = kmeans(RegMat_2D, OptK, 'distance', 'City', 'Replicates', 5, 'MaxIter', 150, 'empty', 'drop');
        IDX_itr(:,itr) = IDX;
        C_itr(:,:,itr) = C;
        SUMD_itr(:,itr) = SUMD;
        D_itr(:,:,itr) = D;
    end
    [val, idx_min] = min(sum(SUMD_itr));
    
    cluster_results{Nsub,1}.OptK = OptK;
    cluster_results{Nsub,1}.IDX = IDX_itr(:,idx_min);
    cluster_results{Nsub,1}.C = C_itr(:,:,idx_min);
    cluster_results{Nsub,1}.SUMD = SUMD_itr(:,idx_min);
    cluster_results{Nsub,1}.D = D_itr(:,:,idx_min);
end
cd(ConnPath);
save('cluster_results.mat','cluster_results');


%%%%%%%%%%%%%%%%%%% Perform group-wise clustering with optimal k %%%%%%%%%%%%%%%%%%%
total_OptK = 0;
for Nsub = 1:length(flist)
    total_OptK = [total_OptK; cluster_results{Nsub,1}.OptK];
end
total_OptK(1,:) = [];
FinOptK = mode(total_OptK);

RegMat_AllCat = [];
for Nsub = 1:length(flist)
    RegMat_3D = RegMat_stack{Nsub,1};
    RegMat_AllCat = cat(3, RegMat_AllCat, RegMat_3D);
end
RegMat_AllCat_2D = reshape(RegMat_AllCat, Nroi*Nroi, size(RegMat_AllCat,3) )';
IDX = kmeans(RegMat_AllCat_2D, FinOptK);
grp_mean_state = zeros(Nroi, Nroi, FinOptK);
for num_k = 1:FinOptK
    state = RegMat_AllCat_2D(find(IDX == num_k),:);
    mean_state = reshape(mean(state), [Nroi Nroi]);
    grp_mean_state(:,:,num_k) = mean_state;
end
cd(ConnPath);
save('FinOptK.mat','FinOptK');
save('grp_mean_state.mat', 'grp_mean_state');

for num_k = 1:FinOptK
    figure('Position',[200 400 400 400]);
    imagesc(grp_mean_state(:,:,num_k)); caxis([-0.1 0.1]);
    title({strcat(['State ',int2str(num_k)])},'fontsize',15, 'fontweight', 'bold');
    axis off;
end

%%%%%%%%%%%%%%%%%%% Perform subject-wise clustering with optimal k %%%%%%%%%%%%%%%%%%%
Params_states = cell(length(flist),1);
for Nsub = 1:length(flist)
    disp(strcat(['Subject: ',int2str(Nsub)]));
    
    RegMat_3D = RegMat_stack{Nsub,1};
    RegMat_2D = reshape(RegMat_3D,[size(RegMat_3D,1)*size(RegMat_3D,2), size(RegMat_3D,3)])';
    BC_2D = netparam_stack{Nsub,2};
    DC_2D = netparam_stack{Nsub,3};
    EVC_2D = netparam_stack{Nsub,4};
    
    IDX = kmeans(RegMat_2D,FinOptK);
    
    Nwin = size(RegMat_3D,3);
    % 1: RegMat
    % 2~4: BC, DC, EVC
    for num_states = 1:FinOptK
        inds = find(IDX == num_states);
        Params_states{Nsub,1}{1, num_states} = RegMat_3D(:,:,inds);
        Params_states{Nsub,1}{2, num_states} = BC_2D(inds,:);
        Params_states{Nsub,1}{3, num_states} = DC_2D(inds,:);
        Params_states{Nsub,1}{4, num_states} = EVC_2D(inds,:);
    end
end
cd(ConnPath);
save('Params_states.mat','Params_states');


%%%%%%%%%%%%%%%%%%% Compare subject-wise and group-wise states %%%%%%%%%%%%%%%%%%%
all_subj_state = zeros(length(flist), FinOptK);
% Columns: Each subject's state
% Elements: Reference state (group state)
for Nsub = 1:length(flist)
    subj_mean_state = zeros(Nroi,Nroi,FinOptK);
    for num_k = 1:FinOptK
        subj_mean_state(:,:,num_k) = mean(Params_states{Nsub,1}{1,num_k},3);
    end
    
    relation = zeros(FinOptK,FinOptK);
    for i = 1:FinOptK
        for j = 1:FinOptK
            temp1 = grp_mean_state(:,:,i);
            temp2 = subj_mean_state(:,:,j);
            [r p] = corr(temp1(:), temp2(:));
            relation(i,j) = r;
        end
    end
    
    CompState = zeros(2,FinOptK);
    [val idx] = max(abs(relation),[],2);
    CompState(1,:) = idx;
    CompState(2,:) = val;
    
    ref = unique(CompState(1,:));
    for num_ref = 1:length(ref)
        temp = CompState(2, find(CompState(1,:) == ref(num_ref)));
        subj_state = find(CompState(2,:) == max(temp));
        all_subj_state(Nsub, subj_state) = CompState(1,subj_state);
    end
    
    while 1
        sub_st = unique(all_subj_state(Nsub,:));
        if sub_st(1) ~= 0
            break;
        else
            er_ref = unique(all_subj_state(Nsub,:));
            if er_ref(1) == 0
                er_ref(:,1) = [];
            end
            relation(:,er_ref) = 0;
            CompState = zeros(2,FinOptK);
            [val idx] = max(abs(relation),[],2);
            CompState(1,:) = idx;
            CompState(2,:) = val;
            exist_st = find(all_subj_state(Nsub,:) ~= 0);
            CompState(:,exist_st) = 0;
            
            ref = unique(CompState(1,:));
            for num_ref = 1:length(ref)
                temp = CompState(2, find(CompState(1,:) == ref(num_ref)));
                if sum(temp) ~= 0
                    subj_state = find(CompState(2,:) == max(temp));
                    all_subj_state(Nsub, subj_state) = CompState(1,subj_state);
                end
            end
        end
    end
end
cd(ConnPath);
save('all_subj_state.mat','all_subj_state');


%%%%%%%%%%%%%%%%%% Collect mean state matrices across subjects for each group %%%%%%%%%%%%%%%%%%%
Params_states_reorder = cell(length(flist),1);
for Nsub = 1:length(flist)
    for num_states = 1:FinOptK
        idx = find(all_subj_state(Nsub,:) == num_states);
        Params_states_reorder{Nsub,1}{1,num_states} = Params_states{Nsub,1}{1,idx};
        Params_states_reorder{Nsub,1}{2,num_states} = Params_states{Nsub,1}{2,idx};
        Params_states_reorder{Nsub,1}{3,num_states} = Params_states{Nsub,1}{3,idx};
        Params_states_reorder{Nsub,1}{4,num_states} = Params_states{Nsub,1}{4,idx};
    end
end
cd(ConnPath);
save('Params_states_reorder.mat','Params_states_reorder');


%%%%%%%%%%%%%%%%%% Calculate network parameters per state per subject %%%%%%%%%%%%%%%%%%
subj_mean_state = cell(length(flist),1);
for Nsub = 1:length(flist)
    for num_states = 1:FinOptK
        subj_mean_state{Nsub,1}{1,num_states} = mean(Params_states_reorder{Nsub,1}{1,num_states},3);
        
        wrcm = subj_mean_state{Nsub,1}{1,num_states};
        [B, D, E] = NetworkCentrality(wrcm);
        subj_mean_state{Nsub,1}{2,num_states} = B(:,1);
        subj_mean_state{Nsub,1}{3,num_states} = D;
        subj_mean_state{Nsub,1}{4,num_states} = E;
    end
end
cd(ConnPath);
save('subj_mean_state.mat','subj_mean_state');




%%%%%%%%%%%%%%%%%% Group comparison %%%%%%%%%%%%%%%%%%
cd(ConnPath_temp_result);
for num_states = 1:FinOptK
    disp(strcat(['State: ',int2str(num_states)]));
    
    %%%%% Real network parameters per state
    grp1_netparam = zeros(Nroi,3,grp1_case);
    grp2_netparam = zeros(Nroi,3,grp2_case);
    % 1:BC, 2:DC, 3:EVC
    for Nsub = 1:grp1_case
        grp1_netparam(:,1,Nsub) = subj_mean_state{Nsub,1}{2,num_states};
        grp1_netparam(:,2,Nsub) = subj_mean_state{Nsub,1}{3,num_states};
        grp1_netparam(:,3,Nsub) = subj_mean_state{Nsub,1}{4,num_states};
    end
    for Nsub = 1:grp2_case
        grp2_netparam(:,1,Nsub) = subj_mean_state{grp1_case+Nsub,1}{2,num_states};
        grp2_netparam(:,2,Nsub) = subj_mean_state{grp1_case+Nsub,1}{3,num_states};
        grp2_netparam(:,3,Nsub) = subj_mean_state{grp1_case+Nsub,1}{4,num_states};
    end
    save(strcat('state',int2str(num_states),'_',grp1_name,'_netparam.mat'),'grp1_netparam');
    save(strcat('state',int2str(num_states),'_',grp2_name,'_netparam.mat'),'grp2_netparam');
    
    %%%%% Permutation
    total_netparam = cat(3,grp1_netparam, grp2_netparam);
    grp1_netparam_perm = zeros(Nroi, 3, grp1_case, num_perm);
    grp2_netparam_perm = zeros(Nroi, 3, grp2_case, num_perm);
    for Nperm = 1:num_perm
        r = randperm(total_case);
        grp1_netparam_perm(:,:,:,Nperm) = total_netparam(:,:,r(1:grp1_case));
        grp2_netparam_perm(:,:,:,Nperm) = total_netparam(:,:,r(1+grp1_case:total_case));
    end
    save(strcat('state',int2str(num_states),'_',grp1_name,'_netparam_perm.mat'),'grp1_netparam_perm');
    save(strcat('state',int2str(num_states),'_',grp2_name,'_netparam_perm.mat'),'grp2_netparam_perm');
    
    %%%%% Real difference
    real_diff_netparam = zeros(Nroi,6);
    % 1st: BC (grp1 > grp2), 2nd: BC (grp1 < grp2)
    % 3rd: DEG (grp1 > grp2), 4th: DEG (grp1 < grp2)
    % 5th: EVC (grp1 > grp2), 6th: EVC (grp1 < grp2)
    
    grp1_netparam_mean = mean(grp1_netparam, 3);
    grp2_netparam_mean = mean(grp2_netparam, 3);    
    for i = 1:Nroi
        % BC
        real_diff_netparam(i,1) = grp1_netparam_mean(i,1) - grp2_netparam_mean(i,1);
        if real_diff_netparam(i,1) < 0
            real_diff_netparam(i,1) = 0;
        end
        real_diff_netparam(i,2) = grp2_netparam_mean(i,1) - grp1_netparam_mean(i,1);
        if real_diff_netparam(i,2) < 0
            real_diff_netparam(i,2) = 0;
        end
        
        % DEG
        real_diff_netparam(i,3) = grp1_netparam_mean(i,2) - grp2_netparam_mean(i,2);
        if real_diff_netparam(i,3) < 0
            real_diff_netparam(i,3) = 0;
        end
        real_diff_netparam(i,4) = grp2_netparam_mean(i,2) - grp1_netparam_mean(i,2);
        if real_diff_netparam(i,4) < 0
            real_diff_netparam(i,4) = 0;
        end
        
        % EVC
        real_diff_netparam(i,5) = grp1_netparam_mean(i,3) - grp2_netparam_mean(i,3);
        if real_diff_netparam(i,5) < 0
            real_diff_netparam(i,5) = 0;
        end
        real_diff_netparam(i,6) = grp2_netparam_mean(i,3) - grp1_netparam_mean(i,3);
        if real_diff_netparam(i,6) < 0
            real_diff_netparam(i,6) = 0;
        end
    end
    save(strcat('state',int2str(num_states),'_real_diff_netparam.mat'),'real_diff_netparam');
    
    %%%%% Perm difference
    perm_diff_netparam = zeros(Nroi,6,num_perm);
    grp1_netparam_perm_mean = reshape(mean(grp1_netparam_perm,3), [size(grp1_netparam_perm,1), size(grp1_netparam_perm,2), size(grp1_netparam_perm, 4)]);
    grp2_netparam_perm_mean = reshape(mean(grp2_netparam_perm,3), [size(grp2_netparam_perm,1), size(grp2_netparam_perm,2), size(grp2_netparam_perm, 4)]);
    for j = 1:num_perm
        % BC
        for i = 1:Nroi
            perm_diff_netparam(i,1,j) = grp1_netparam_perm_mean(i,1,j) - grp2_netparam_perm_mean(i,1,j);
            if perm_diff_netparam(i,1,j) < 0
                perm_diff_netparam(i,1,j) = 0;
            end
        end
        for i = 1:Nroi
            perm_diff_netparam(i,2,j) = grp2_netparam_perm_mean(i,1,j) - grp1_netparam_perm_mean(i,1,j);
            if perm_diff_netparam(i,2,j) < 0
                perm_diff_netparam(i,2,j) = 0;
            end
        end
        
        % DEG
        for i = 1:Nroi
            perm_diff_netparam(i,3,j) = grp1_netparam_perm_mean(i,2,j) - grp2_netparam_perm_mean(i,2,j);
            if perm_diff_netparam(i,3,j) < 0
                perm_diff_netparam(i,3,j) = 0;
            end
        end
        for i = 1:Nroi
            perm_diff_netparam(i,4,j) = grp2_netparam_perm_mean(i,2,j) - grp1_netparam_perm_mean(i,2,j);
            if perm_diff_netparam(i,4,j) < 0
                perm_diff_netparam(i,4,j) = 0;
            end
        end
        
        % EVC
        for i = 1:Nroi
            perm_diff_netparam(i,5,j) = grp1_netparam_perm_mean(i,3,j) - grp2_netparam_perm_mean(i,3,j);
            if perm_diff_netparam(i,5,j) < 0
                perm_diff_netparam(i,5,j) = 0;
            end
        end
        for i = 1:Nroi
            perm_diff_netparam(i,6,j) = grp2_netparam_perm_mean(i,3,j) - grp1_netparam_perm_mean(i,3,j);
            if perm_diff_netparam(i,6,j) < 0
                perm_diff_netparam(i,6,j) = 0;
            end
        end
    end
    save(strcat('state',int2str(num_states),'_perm_diff_netparam.mat'),'perm_diff_netparam');
    
    %%%%%%%%%%%%%%%%%%%% statistics %%%%%%%%%%%%%%%%%%%%
    pval_result_uncor = zeros(Nroi,6);
    % BC
    for i = 1:Nroi
        pval_result_uncor(i,1) = sum(abs(perm_diff_netparam(i,1,:)) >= abs(real_diff_netparam(i,1))) / length(perm_diff_netparam(i,1,:));
    end
    for i = 1:Nroi
        pval_result_uncor(i,2) = sum(abs(perm_diff_netparam(i,2,:)) >= abs(real_diff_netparam(i,2))) / length(perm_diff_netparam(i,2,:));
    end
    
    % DEG
    for i = 1:Nroi
        pval_result_uncor(i,3) = sum(abs(perm_diff_netparam(i,3,:)) >= abs(real_diff_netparam(i,3))) / length(perm_diff_netparam(i,3,:));
    end
    for i = 1:Nroi
        pval_result_uncor(i,4) = sum(abs(perm_diff_netparam(i,4,:)) >= abs(real_diff_netparam(i,4))) / length(perm_diff_netparam(i,4,:));
    end
    
    % EVC
    for i = 1:Nroi
        pval_result_uncor(i,5) = sum(abs(perm_diff_netparam(i,5,:)) >= abs(real_diff_netparam(i,5))) / length(perm_diff_netparam(i,5,:));
    end
    for i = 1:Nroi
        pval_result_uncor(i,6) = sum(abs(perm_diff_netparam(i,6,:)) >= abs(real_diff_netparam(i,6))) / length(perm_diff_netparam(i,6,:));
    end
    
    pval_result_uncor_remained = pval_result_uncor;
    for i = 1:Nroi
        for j = 1:6
            if pval_result_uncor_remained(i,j) > 0.05
                pval_result_uncor_remained(i,j) = 1;
            end
        end
    end
    
    save(strcat('state',int2str(num_states),'_pval_result_uncor.mat'),'pval_result_uncor');
    save(strcat('state',int2str(num_states),'_pval_result_uncor_remained.mat'),'pval_result_uncor_remained');
    
    a = find(pval_result_uncor_remained(:,1) ~= 1);
    b = find(pval_result_uncor_remained(:,2) ~= 1);
    sig_regions_BC_uncor = vertcat(a,b);
    sig_regions_BC_uncor = sort(sig_regions_BC_uncor);
    save(strcat('state',int2str(num_states),'_sig_regions_BC_uncor.mat'),'sig_regions_BC_uncor');
    
    a = find(pval_result_uncor_remained(:,3) ~= 1);
    b = find(pval_result_uncor_remained(:,4) ~= 1);
    sig_regions_DEG_uncor = vertcat(a,b);
    sig_regions_DEG_uncor = sort(sig_regions_DEG_uncor);
    save(strcat('state',int2str(num_states),'_sig_regions_DEG_uncor.mat'),'sig_regions_DEG_uncor');
    
    a = find(pval_result_uncor_remained(:,5) ~= 1);
    b = find(pval_result_uncor_remained(:,6) ~= 1);
    sig_regions_EVC_uncor = vertcat(a,b);
    sig_regions_EVC_uncor = sort(sig_regions_EVC_uncor);
    save(strcat('state',int2str(num_states),'_sig_regions_EVC_uncor.mat'),'sig_regions_EVC_uncor');
    
    
    
    p_cor_permFDR = zeros(Nroi, 3);
    
    % BC
    p_uncor = min(pval_result_uncor(:,1:2)')';
    p_cor = sort(p_uncor);
    for j = 1:length(p_cor)
        p_cor(j,2) = (size(p_cor,1) - j + 1) * p_cor(j,1);
        if p_cor(j,2) > 1
            p_cor(j,2) = 1;
        end
        p_uncor(find(p_cor(j,1) == p_uncor(:,1)), 2) = p_cor(j,2);
    end
    [h, crit_p, adj_ci_cvrg, adj_p]=fdr_bh(p_uncor(:,1),0.05);   p_uncor(:,3) = adj_p;
    % p_uncor: col 1: uncorrected / col 2: HB corrected / col 3: FDR corrected
    p_cor_permFDR(:,1) = p_uncor(:,3);
    sig_regions_BC_permFDR = find(p_cor_permFDR(:,1) < 0.05);
    save(strcat('state',int2str(num_states),'_sig_regions_BC_permFDR.mat'), 'sig_regions_BC_permFDR');
    
    % DEG
    p_uncor = min(pval_result_uncor(:,3:4)')';
    p_cor = sort(p_uncor);
    for j = 1:length(p_cor)
        p_cor(j,2) = (size(p_cor,1) - j + 1) * p_cor(j,1);
        if p_cor(j,2) > 1
            p_cor(j,2) = 1;
        end
        p_uncor(find(p_cor(j,1) == p_uncor(:,1)), 2) = p_cor(j,2);
    end
    [h, crit_p, adj_ci_cvrg, adj_p]=fdr_bh(p_uncor(:,1),0.05);   p_uncor(:,3) = adj_p;
    % p_uncor: col 1: uncorrected / col 2: HB corrected / col 3: FDR corrected
    p_cor_permFDR(:,2) = p_uncor(:,3);
    sig_regions_DEG_permFDR = find(p_cor_permFDR(:,2) < 0.05);
    save(strcat('state',int2str(num_states),'_sig_regions_DEG_permFDR.mat'), 'sig_regions_DEG_permFDR');
    
    % EVC
    p_uncor = min(pval_result_uncor(:,5:6)')';
    p_cor = sort(p_uncor);
    for j = 1:length(p_cor)
        p_cor(j,2) = (size(p_cor,1) - j + 1) * p_cor(j,1);
        if p_cor(j,2) > 1
            p_cor(j,2) = 1;
        end
        p_uncor(find(p_cor(j,1) == p_uncor(:,1)), 2) = p_cor(j,2);
    end
    [h, crit_p, adj_ci_cvrg, adj_p]=fdr_bh(p_uncor(:,1),0.05);   p_uncor(:,3) = adj_p;
    % p_uncor: col 1: uncorrected / col 2: HB corrected / col 3: FDR corrected
    p_cor_permFDR(:,3) = p_uncor(:,3);
    sig_regions_EVC_permFDR = find(p_cor_permFDR(:,3) < 0.05);
    save(strcat('state',int2str(num_states),'_sig_regions_EVC_permFDR.mat'), 'sig_regions_EVC_permFDR');
    
    save(strcat('state',int2str(num_states),'_p_cor_permFDR.mat'), 'p_cor_permFDR');
end
